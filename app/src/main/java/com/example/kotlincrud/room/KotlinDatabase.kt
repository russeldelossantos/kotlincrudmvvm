package com.example.kotlincrud.room

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.kotlincrud.dao.*
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.model.Employee

@Database(
    version = 2,
    entities = [
        Admin::class,
        Customer::class,
        Employee::class
    ],
    exportSchema = false)

abstract class KotlinDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private lateinit var INSTANCE: KotlinDatabase

        fun getDatabase(context: Context): KotlinDatabase {
            if (!::INSTANCE.isInitialized)
                INSTANCE = androidx.room.Room.databaseBuilder(
                    context.applicationContext,
                    KotlinDatabase::class.java,
                    "kotlin_database.db"
                ).build()

            return INSTANCE
        }
    }
    
    abstract fun getAdminDao():AdminDao
    abstract fun getEmployeeDao():EmployeeDao
    abstract fun getCustomerDao():CustomerDao

}