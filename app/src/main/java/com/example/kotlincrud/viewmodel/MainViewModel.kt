package com.example.kotlincrud.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.kotlincrud.dialogs.AdminDialog
import com.example.kotlincrud.dialogs.CustomerDialog
import com.example.kotlincrud.dialogs.EmployeeDialog
import com.example.kotlincrud.dialogs.SelectionDialog
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.model.Employee
import com.example.kotlincrud.room.KotlinDatabase

class MainViewModel(var context: Context):ViewModel() {

    private val dao : KotlinDatabase = KotlinDatabase.getDatabase(context)
    private lateinit var adminDialog: AdminDialog
    private lateinit var customerDialog: CustomerDialog
    private lateinit var employeeDialog: EmployeeDialog

    fun getAllAdmin() : LiveData<List<Admin>>{
        return dao.getAdminDao().getAllAdmin()
    }

    fun getAllCustomer() : LiveData<List<Customer>>{
        return dao.getCustomerDao().getAllCustomer()
    }

    fun getAllEmployee() : LiveData<List<Employee>>{
        return dao.getEmployeeDao().getAllEmployee()
    }

    fun <T> showUpdateDialog(type:String, model: T){
        dialogType(type,"update",model)
    }

    fun showSelectionDialog(){
        val selectionDialog = SelectionDialog(context)
        selectionDialog.show()
    }

    fun showSelectedDialog(type:String){
      dialogType(type,"add",null)
    }

    private fun <T> dialogType(type: String, key:String, model:T){
        when(type){
            "admin"->{
                adminDialog = AdminDialog(context,key)
                adminDialog.show()
                if (isUpdate(key))
                adminDialog.setTextField(model as Admin)
            }
            "customer"->{
                customerDialog = CustomerDialog(context,key)
                customerDialog.show()
                if (isUpdate(key))
                    customerDialog.setTextField(model as Customer)
            }
            "employee"->{
                employeeDialog = EmployeeDialog(context,key)
                employeeDialog.show()
                if (isUpdate(key))
                    employeeDialog.setTextField(model as Employee)
            }
        }
    }

    private fun isUpdate(key: String): Boolean{
        return key=="update"
    }

}