package com.example.kotlincrud.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kotlincrud.model.Employee
import com.example.kotlincrud.respository.EmployeeRepository
import com.example.kotlincrud.room.KotlinDatabase
import com.example.kotlincrud.helper.RandomStringNumberHelper
import kotlinx.coroutines.launch

class EmployeeDialogViewModel(var context: Context):ViewModel() {

   private var employeeRepository: EmployeeRepository

    init {
        val kotlinDatabase = KotlinDatabase.getDatabase(context)
        employeeRepository = EmployeeRepository(kotlinDatabase)
    }

      fun addEmployee(employee: Employee) = viewModelScope.launch{
          employeeRepository.insert(employee)
          Log.d("rds", "addEmployee ${employee.id} | ${employee.name} | ${employee.phone}")
          Toast.makeText(context, "Add Success.", Toast.LENGTH_SHORT).show()
     }

     fun updateEmployee(employee: Employee) = viewModelScope.launch{
         employeeRepository.update(employee)
         Log.d("rds", "updateEmployee: ${employee.id} | ${employee.name} | ${employee.phone}")
         Toast.makeText(context, "Update Success.", Toast.LENGTH_SHORT).show()
    }

    fun deleteEmployee(employee: Employee) = viewModelScope.launch{
        employeeRepository.delete(employee)
        Toast.makeText(context, "Delete Success.", Toast.LENGTH_SHORT).show()
    }

    fun autoGenerateEmployee() = viewModelScope.launch{
        employeeRepository.insert(Employee(0,
                RandomStringNumberHelper.generateRandomCharacters(20),
            RandomStringNumberHelper.generateRandomNumbers(11)))
        Toast.makeText(context, "Add Success.", Toast.LENGTH_SHORT).show()
    }

}