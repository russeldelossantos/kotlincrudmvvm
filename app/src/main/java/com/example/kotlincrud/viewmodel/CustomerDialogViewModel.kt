package com.example.kotlincrud.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.respository.CustomerRepository
import com.example.kotlincrud.room.KotlinDatabase
import com.example.kotlincrud.helper.RandomStringNumberHelper
import kotlinx.coroutines.launch

class CustomerDialogViewModel(var context: Context):ViewModel() {

   private var customerRepository: CustomerRepository

    init {
        val kotlinDatabase = KotlinDatabase.getDatabase(context)
        customerRepository = CustomerRepository(kotlinDatabase)
    }

      fun addCustomer(customer: Customer) = viewModelScope.launch{
          customerRepository.insert(customer)
          Log.d("rds", "addCustomer: ${customer.id} | ${customer.name} | ${customer.phone}")
          Toast.makeText(context, "Add Success.", Toast.LENGTH_SHORT).show()
     }

     fun updateCustomer(customer: Customer) = viewModelScope.launch{
         customerRepository.update(customer)
         Log.d("rds", "updateCustomer: ${customer.id} | ${customer.name} | ${customer.phone}")
         Toast.makeText(context, "Update Success.", Toast.LENGTH_SHORT).show()
    }

    fun deleteCustomer(customer: Customer) = viewModelScope.launch{
        customerRepository.delete(customer)
        Toast.makeText(context, "Delete Success.", Toast.LENGTH_SHORT).show()
    }

    fun autoGenerateCustomer() = viewModelScope.launch{
        customerRepository.insert(Customer(0,
            RandomStringNumberHelper.generateRandomCharacters(20),
            RandomStringNumberHelper.generateRandomNumbers(11)))
        Toast.makeText(context, "Add Success.", Toast.LENGTH_SHORT).show()
    }

}