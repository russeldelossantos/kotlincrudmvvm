package com.example.kotlincrud.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.respository.AdminRepository
import com.example.kotlincrud.room.KotlinDatabase
import com.example.kotlincrud.helper.RandomStringNumberHelper
import kotlinx.coroutines.launch

class AdminDialogViewModel(var context: Context):ViewModel() {

    private var adminRepository: AdminRepository

    init {
        val kotlinDatabase = KotlinDatabase.getDatabase(context)
        adminRepository = AdminRepository(kotlinDatabase)
    }

      fun addAdmin(admin: Admin) = viewModelScope.launch{
          adminRepository.insert(admin)
          Log.d("rds", "addAdmin: ${admin.id} | ${admin.name} | ${admin.phone}")
          Toast.makeText(context, "Add Success.", Toast.LENGTH_SHORT).show()
     }

     fun updateAdmin(admin: Admin) = viewModelScope.launch{
         adminRepository.update(admin)
         Log.d("rds", "updateAdmin: ${admin.id} | ${admin.name} | ${admin.phone}")
         Toast.makeText(context, "Update Success.", Toast.LENGTH_SHORT).show()
    }

    fun deleteAdmin(admin: Admin) = viewModelScope.launch{
        adminRepository.delete(admin)
        Toast.makeText(context, "Delete Success.", Toast.LENGTH_SHORT).show()
    }

    fun autoGenerateAdmin() = viewModelScope.launch{
        adminRepository.insert(Admin(0,
            RandomStringNumberHelper.generateRandomCharacters(20),RandomStringNumberHelper.generateRandomNumbers(11)))
        Toast.makeText(context, "Add Success.", Toast.LENGTH_SHORT).show()
    }

}