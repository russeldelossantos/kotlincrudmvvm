package com.example.kotlincrud.dialogs

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.kotlincrud.R
import com.example.kotlincrud.databinding.ViewEmployeeDialogBinding
import com.example.kotlincrud.model.Employee
import com.example.kotlincrud.viewmodel.EmployeeDialogViewModel
import kotlinx.android.synthetic.main.view_admin_dialog.*

class EmployeeDialog(context:Context, val type:String): Dialog(context),View.OnClickListener {

    private var dialogMainBinding: ViewEmployeeDialogBinding?=null
    private lateinit var employee: Employee
    private var employeeDialogViewModel: EmployeeDialogViewModel

    init {
        dialogMainBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext())
            ,R.layout.view_employee_dialog,null,false) as ViewEmployeeDialogBinding
        setContentView(dialogMainBinding!!.root)
        employeeDialogViewModel = EmployeeDialogViewModel(context)
        val window: Window? = window
        window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        dialogMainBinding!!.employeeDialogViewModel = employeeDialogViewModel
        dialogMainBinding!!.btnDelete.visibility = View.GONE
        dialogMainBinding!!.btnAdd.setOnClickListener(this)
        dialogMainBinding!!.btnAutoAdd.setOnClickListener(this)
        dialogMainBinding!!.btnDelete.setOnClickListener(this)
        dialogMainBinding?.tvEmployeeTitle?.text = "Add Employee"
    }

    override fun onClick(view: View) {
        when(view.id){
            R.id.btn_add -> {
                val name = et_name.text.toString()
                val phone = et_phone.text.toString()
                when(type){
                    "add" -> if (!fieldIsEmpty(name) || !fieldIsEmpty(phone)) {
                        employeeDialogViewModel.addEmployee(Employee(0,name,phone))
                        dismiss()
                    } else{
                        Toast.makeText(context, "Fields are Empty!", Toast.LENGTH_SHORT).show()
                    }

                    "update" -> if (!fieldIsEmpty(name) || !fieldIsEmpty(phone)) {
                        employeeDialogViewModel.updateEmployee(Employee(employee.id,name,phone))
                        Log.d("rds", "update: ${employee.id} | ${employee.name} | ${employee.phone}")
                        dismiss()
                    }  else{
                        Toast.makeText(context, "Fields are Empty!", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            R.id.btn_delete -> {
                employeeDialogViewModel.deleteEmployee(employee)
                dismiss()
            }

            R.id.btn_autoAdd -> {
                employeeDialogViewModel.autoGenerateEmployee()
                dismiss()
            }

        }
    }

    fun setTextField(employee: Employee){
        this.employee = employee
        dialogMainBinding?.etName?.setText(employee.name)
        dialogMainBinding?.etPhone?.setText(employee.phone)
        dialogMainBinding?.btnAdd?.text = "Update"
        dialogMainBinding?.tvEmployeeTitle?.text = "Update Employee"
        dialogMainBinding!!.btnDelete.visibility = View.VISIBLE
    }

  private fun fieldIsEmpty(value:String): Boolean{
        return value.isEmpty()
    }

}