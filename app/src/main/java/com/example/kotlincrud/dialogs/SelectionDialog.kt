package com.example.kotlincrud.dialogs

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.example.kotlincrud.R
import com.example.kotlincrud.databinding.ViewSelectionBinding
import com.example.kotlincrud.viewmodel.MainViewModel

class SelectionDialog(context:Context): Dialog(context),View.OnClickListener {

    private var dialogMainBinding: ViewSelectionBinding?=null
    private var mainViewModel : MainViewModel

    init {
        dialogMainBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext())
            ,R.layout.view_selection,null,false) as ViewSelectionBinding
        setContentView(dialogMainBinding!!.root)
        mainViewModel = MainViewModel(context)
        val window: Window? = window
        window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        dialogMainBinding!!.selectionViewModel = mainViewModel
        dialogMainBinding!!.btnAdmin.setOnClickListener(this)
        dialogMainBinding!!.btnCustomer.setOnClickListener(this)
        dialogMainBinding!!.btnEmployee.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id){
            R.id.btn_admin -> {
                mainViewModel.showSelectedDialog("admin")
                dismiss()
            }
            R.id.btn_customer -> {
                mainViewModel.showSelectedDialog("customer")
                dismiss()
            }
            R.id.btn_employee -> {
                mainViewModel.showSelectedDialog("employee")
                dismiss()
            }
        }
    }
}
