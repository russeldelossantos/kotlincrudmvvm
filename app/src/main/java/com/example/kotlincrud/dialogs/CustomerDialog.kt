package com.example.kotlincrud.dialogs

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.kotlincrud.R
import com.example.kotlincrud.databinding.ViewCustomerDialogBinding
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.viewmodel.CustomerDialogViewModel
import kotlinx.android.synthetic.main.view_admin_dialog.*

class CustomerDialog(context:Context, val type:String): Dialog(context),View.OnClickListener {

    private var dialogMainBinding: ViewCustomerDialogBinding?=null
    private lateinit var customer: Customer
    private var customerDialogViewModel: CustomerDialogViewModel

    init {
        dialogMainBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext())
            ,R.layout.view_customer_dialog,null,false) as ViewCustomerDialogBinding
        setContentView(dialogMainBinding!!.root)
        customerDialogViewModel = CustomerDialogViewModel(context)
        val window: Window? = window
        window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        dialogMainBinding!!.customerDialogViewModel = customerDialogViewModel
        dialogMainBinding!!.btnDelete.visibility = View.GONE
        dialogMainBinding!!.btnAdd.setOnClickListener(this)
        dialogMainBinding!!.btnAutoAdd.setOnClickListener(this)
        dialogMainBinding!!.btnDelete.setOnClickListener(this)
        dialogMainBinding?.tvCustomerTitle?.text = "Add Customer"
    }

    override fun onClick(view: View) {
        when (view.id){
            R.id.btn_add -> {
                val name = et_name.text.toString()
                val phone = et_phone.text.toString()
                when(type){
                    "add" -> if (!fieldIsEmpty(name) || !fieldIsEmpty(phone)) {
                        customerDialogViewModel.addCustomer(Customer(0,name,phone))
                        dismiss()
                    } else{
                        Toast.makeText(context, "Fields are Empty!", Toast.LENGTH_SHORT).show()
                    }

                    "update" -> if (!fieldIsEmpty(name) || !fieldIsEmpty(phone)) {
                        customerDialogViewModel.updateCustomer(Customer(customer.id,name,phone))
                        Log.d("rds", "update: ${customer.id} | ${customer.name} | ${customer.phone}")
                        dismiss()
                    }  else{
                        Toast.makeText(context, "Fields are Empty!", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            R.id.btn_delete ->{
                customerDialogViewModel.deleteCustomer(customer)
                dismiss()
            }

            R.id.btn_autoAdd -> {
                customerDialogViewModel.autoGenerateCustomer()
                dismiss()
            }

        }
    }

    fun setTextField(customer: Customer){
        this.customer = customer
        dialogMainBinding?.etName?.setText(customer.name)
        dialogMainBinding?.etPhone?.setText(customer.phone)
        dialogMainBinding?.btnAdd?.text = "Update"
        dialogMainBinding?.tvCustomerTitle?.text = "Update Customer"
        dialogMainBinding!!.btnDelete.visibility = View.VISIBLE
    }

  private fun fieldIsEmpty(value:String): Boolean{
        return value.isEmpty()
    }

}