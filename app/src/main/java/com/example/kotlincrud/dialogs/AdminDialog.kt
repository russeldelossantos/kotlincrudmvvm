package com.example.kotlincrud.dialogs

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.kotlincrud.R
import com.example.kotlincrud.databinding.ViewAdminDialogBinding
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.viewmodel.AdminDialogViewModel
import kotlinx.android.synthetic.main.view_admin_dialog.*

class AdminDialog(context:Context, val type:String): Dialog(context),View.OnClickListener {

    private var dialogMainBinding: ViewAdminDialogBinding?=null
    private lateinit var admin: Admin
    private val adminDialogViewModel: AdminDialogViewModel

    init {
        dialogMainBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext())
            ,R.layout.view_admin_dialog,null,false) as ViewAdminDialogBinding
        setContentView(dialogMainBinding!!.root)
        adminDialogViewModel = AdminDialogViewModel(context)
        val window: Window? = window
        window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        dialogMainBinding!!.adminDialogViewModel = adminDialogViewModel
        dialogMainBinding!!.btnDelete.visibility = View.GONE
        dialogMainBinding!!.btnAdd.setOnClickListener(this)
        dialogMainBinding!!.btnAutoAdd.setOnClickListener(this)
        dialogMainBinding!!.btnDelete.setOnClickListener(this)
        dialogMainBinding?.tvAdminTitle?.text = "Add Admin"
    }

    override fun onClick(view: View) {
        val name = et_name.text.toString()
        val phone = et_phone.text.toString()
        when(view.id){
            R.id.btn_add -> {
                when(type){
                    "add" -> if (!fieldIsEmpty(name) || !fieldIsEmpty(phone)) {
                        adminDialogViewModel.addAdmin(Admin(0,name,phone))
                        dismiss()
                    } else{
                        Toast.makeText(context, "Fields are Empty!", Toast.LENGTH_SHORT).show()
                    }

                    "update" -> if (!fieldIsEmpty(name) || !fieldIsEmpty(phone)) {
                        adminDialogViewModel.updateAdmin(Admin(admin.id,name,phone))
                        Log.d("rds", "update: ${admin.id} | ${admin.name} | ${admin.phone}")
                        dismiss()
                    }  else{
                        Toast.makeText(context, "Fields are Empty!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            R.id.btn_delete -> {
                adminDialogViewModel.deleteAdmin(admin)
                dismiss()
            }

            R.id.btn_autoAdd -> {
                adminDialogViewModel.autoGenerateAdmin()
                dismiss()
            }

        }
    }

    fun setTextField(admin: Admin){
        this.admin = admin
        dialogMainBinding?.etName?.setText(admin.name)
        dialogMainBinding?.etPhone?.setText(admin.phone)
        dialogMainBinding?.btnAdd?.text = "Update"
        dialogMainBinding?.tvAdminTitle?.text = "Update Admin"
        dialogMainBinding!!.btnDelete.visibility = View.VISIBLE
    }

  private fun fieldIsEmpty(value:String): Boolean{
        return value.isEmpty()
    }

}