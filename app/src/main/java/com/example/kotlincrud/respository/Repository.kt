package com.example.kotlincrud.respository

import com.example.kotlincrud.dao.BaseDao

abstract class Repository<MODEL, DAO: BaseDao<MODEL>>(protected val dao: DAO) {

    open suspend fun insert(vararg items: MODEL) {
        dao.insert(*items)
    }

    open suspend fun update(vararg items: MODEL) {
        dao.update(*items)
    }

    open suspend fun delete(vararg items: MODEL) {
        dao.delete(*items)
    }

}