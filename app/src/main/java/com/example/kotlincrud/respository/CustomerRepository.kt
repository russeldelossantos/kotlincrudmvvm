package com.example.kotlincrud.respository

import com.example.kotlincrud.dao.CustomerDao
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.room.KotlinDatabase

class CustomerRepository(kotlinDatabase: KotlinDatabase)
    :Repository<Customer,CustomerDao>(kotlinDatabase.getCustomerDao()){

    override suspend fun insert(vararg items: Customer) {
        super.insert(*items)
    }

    override suspend fun update(vararg items: Customer) {
        super.update(*items)
    }

    override suspend fun delete(vararg items: Customer) {
        super.delete(*items)
    }

}