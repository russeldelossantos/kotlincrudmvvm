package com.example.kotlincrud.respository

import com.example.kotlincrud.dao.AdminDao
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.room.KotlinDatabase

class AdminRepository(kotlinDatabase: KotlinDatabase)
    :Repository<Admin,AdminDao>(kotlinDatabase.getAdminDao()) {

    override suspend fun insert(vararg items: Admin) {
        super.insert(*items)
    }

    override suspend fun update(vararg items: Admin) {
        super.update(*items)
    }

    override suspend fun delete(vararg items: Admin) {
        super.delete(*items)
    }

}