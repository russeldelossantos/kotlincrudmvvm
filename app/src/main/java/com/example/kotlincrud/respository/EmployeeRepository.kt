package com.example.kotlincrud.respository

import com.example.kotlincrud.dao.EmployeeDao
import com.example.kotlincrud.model.Employee
import com.example.kotlincrud.room.KotlinDatabase

class EmployeeRepository(kotlinDatabase: KotlinDatabase)
    :Repository<Employee,EmployeeDao>(kotlinDatabase.getEmployeeDao()) {

    override suspend fun insert(vararg items: Employee) {
        super.insert(*items)
    }

    override suspend fun update(vararg items: Employee) {
        super.update(*items)
    }

    override suspend fun delete(vararg items: Employee) {
        super.delete(*items)
    }

}