package com.example.kotlincrud.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "customer")
@Parcelize
data class Customer (
    @PrimaryKey(autoGenerate = true) var id:Long=0,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "phone") var phone: String) : Parcelable