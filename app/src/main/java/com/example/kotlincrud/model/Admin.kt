package com.example.kotlincrud.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "admin")
@Parcelize
data class Admin (
    @PrimaryKey(autoGenerate = true) var id:Long = 0,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "phone") var phone: String) : Parcelable
