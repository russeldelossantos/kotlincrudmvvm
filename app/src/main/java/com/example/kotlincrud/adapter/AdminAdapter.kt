package com.example.kotlincrud.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlincrud.R
import com.example.kotlincrud.model.Admin

class AdminAdapter internal constructor(context: Context,onDataListener: OnDataListener) :
    RecyclerView.Adapter<AdminAdapter.AdminViewHolder>() {

    private var adminList = emptyList<Admin>()
    private var onDataListener : OnDataListener = onDataListener

    inner class AdminViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val row: LinearLayout = itemView.findViewById(R.id.ll_row)
        val name: TextView = itemView.findViewById(R.id.tv_name)
        val phone: TextView = itemView.findViewById(R.id.tv_phone)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_list,parent,false)
        return AdminViewHolder(itemView)
    }

    override fun getItemCount(): Int = adminList.size

    override fun onBindViewHolder(holder: AdminViewHolder, position: Int) {
        val admin: Admin = adminList[holder.adapterPosition]
        holder.name.text = admin.name
        holder.phone.text = admin.phone
        holder.row.setOnClickListener {
            if (onDataListener!=null)
            onDataListener.onData(admin)
        }
    }

    internal fun setList(list: List<Admin>){
        this.adminList = list
        notifyDataSetChanged()
    }

    interface OnDataListener{
        fun onData(admin: Admin)
    }

}