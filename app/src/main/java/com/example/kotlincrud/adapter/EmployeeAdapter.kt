package com.example.kotlincrud.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlincrud.R
import com.example.kotlincrud.model.Employee

class EmployeeAdapter internal constructor(context: Context, onDataListener: OnDataListener) :
    RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder>() {

    private var employeeList = emptyList<Employee>()
    private var onDataListener : OnDataListener = onDataListener

    inner class EmployeeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val row: LinearLayout = itemView.findViewById(R.id.ll_row)
        val name: TextView = itemView.findViewById(R.id.tv_name)
        val phone: TextView = itemView.findViewById(R.id.tv_phone)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_list,parent,false)
        return EmployeeViewHolder(itemView)
    }

    override fun getItemCount(): Int = employeeList.size

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val employee: Employee = employeeList[holder.adapterPosition]
        holder.name.text = employee.name
        holder.phone.text = employee.phone
        holder.row.setOnClickListener {
            if (onDataListener!=null)
            onDataListener.onData(employee)
        }
    }

    internal fun setList(list: List<Employee>){
        this.employeeList = list
        notifyDataSetChanged()
    }

    interface OnDataListener{
        fun onData(employee: Employee)
    }

}