package com.example.kotlincrud.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlincrud.R
import com.example.kotlincrud.model.Customer

class CustomerAdapter internal constructor(context: Context, onDataListener: OnDataListener) :
    RecyclerView.Adapter<CustomerAdapter.CustomerViewHolder>() {

    private var customerList = emptyList<Customer>()
    private var onDataListener : OnDataListener = onDataListener

    inner class CustomerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val row: LinearLayout = itemView.findViewById(R.id.ll_row)
        val name: TextView = itemView.findViewById(R.id.tv_name)
        val phone: TextView = itemView.findViewById(R.id.tv_phone)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_list,parent,false)
        return CustomerViewHolder(itemView)
    }

    override fun getItemCount(): Int = customerList.size

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        val customer: Customer = customerList[holder.adapterPosition]
        holder.name.text = customer.name
        holder.phone.text = customer.phone
        holder.row.setOnClickListener {
            if (onDataListener!=null)
            onDataListener.onData(customer)
        }
    }

    internal fun setList(list: List<Customer>){
        this.customerList = list
        notifyDataSetChanged()
    }

    interface OnDataListener{
        fun onData(customer: Customer)
    }

}