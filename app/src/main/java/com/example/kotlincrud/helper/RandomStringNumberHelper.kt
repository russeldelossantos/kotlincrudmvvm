package com.example.kotlincrud.helper

import androidx.annotation.IntRange

class RandomStringNumberHelper {
    companion object{
         fun generateRandomCharacters(@IntRange lenght: Int): String {
            val characters = ('a'..'z') + ('A'..'Z')
            return characters.shuffled().take(lenght).joinToString("")
        }

         fun generateRandomNumbers(@IntRange lenght: Int): String {
            val numeric = ('0'..'9')
            return numeric.shuffled().take(lenght).joinToString("")
        }
    }
}