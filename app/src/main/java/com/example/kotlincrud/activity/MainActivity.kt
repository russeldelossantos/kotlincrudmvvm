package com.example.kotlincrud.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlincrud.R
import com.example.kotlincrud.adapter.AdminAdapter
import com.example.kotlincrud.adapter.CustomerAdapter
import com.example.kotlincrud.adapter.EmployeeAdapter
import com.example.kotlincrud.databinding.ActivityMainBinding
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.model.Employee
import com.example.kotlincrud.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainViewModel = MainViewModel(this)
        fab_add.setOnClickListener {
            activityMainBinding.viewModel!!.showSelectionDialog()
        }
        activityMainBinding.viewModel = mainViewModel
        fetchAdmin()
        fetchCustomer()
        fetchEmployee()
    }

    private fun fetchAdmin(){
        val adminAdapter = AdminAdapter(this,object : AdminAdapter.OnDataListener{
            override fun onData(admin: Admin) {
                mainViewModel.showUpdateDialog("admin",admin)
            }
        })
        rv_admin_list.layoutManager = LinearLayoutManager(this)
        rv_admin_list.adapter = adminAdapter
        mainViewModel.getAllAdmin().observe(this, Observer {
            it?.let {
                adminAdapter.setList(it)
            }
        })
    }

    private fun fetchCustomer(){
        val customerAdapter = CustomerAdapter(this,object : CustomerAdapter.OnDataListener{
            override fun onData(customer: Customer) {
                mainViewModel.showUpdateDialog("customer",customer)
            }
        })
        rv_customer_list.layoutManager = LinearLayoutManager(this)
        rv_customer_list.adapter = customerAdapter
        mainViewModel.getAllCustomer().observe(this, Observer {
            it?.let {
                customerAdapter.setList(it)
            }
        })
    }

    private fun fetchEmployee(){
        val employeeAdapter = EmployeeAdapter(this,object : EmployeeAdapter.OnDataListener{
            override fun onData(employee: Employee) {
                mainViewModel.showUpdateDialog("employee",employee)
            }
        })
        rv_employee_list.layoutManager = LinearLayoutManager(this)
        rv_employee_list.adapter = employeeAdapter
        mainViewModel.getAllEmployee().observe(this, Observer {
            it?.let {
                employeeAdapter.setList(it)
            }
        })
    }

}
