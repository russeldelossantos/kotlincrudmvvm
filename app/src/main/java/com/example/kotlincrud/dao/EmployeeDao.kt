package com.example.kotlincrud.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.model.Employee

@Dao
interface EmployeeDao: BaseDao<Employee> {

    @Query(""" SELECT * FROM employee """)
    fun getAllEmployee(): LiveData<List<Employee>>

}