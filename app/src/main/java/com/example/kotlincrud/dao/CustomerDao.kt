package com.example.kotlincrud.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.kotlincrud.model.Admin
import com.example.kotlincrud.model.Customer
import com.example.kotlincrud.model.Employee

@Dao
interface CustomerDao: BaseDao<Customer> {

    @Query(""" SELECT * FROM customer """)
    fun getAllCustomer(): LiveData<List<Customer>>

}