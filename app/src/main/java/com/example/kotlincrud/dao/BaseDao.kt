package com.example.kotlincrud.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<MODEL> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg items: MODEL)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(vararg items: MODEL)

    @Delete
    suspend fun delete(vararg items: MODEL)

}