package com.example.kotlincrud.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.kotlincrud.model.Admin

@Dao
interface AdminDao: BaseDao<Admin> {

    @Query(""" SELECT * FROM admin """)
    fun getAllAdmin(): LiveData<List<Admin>>

}