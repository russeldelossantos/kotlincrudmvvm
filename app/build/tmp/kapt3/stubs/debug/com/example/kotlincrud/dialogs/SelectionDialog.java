package com.example.kotlincrud.dialogs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/example/kotlincrud/dialogs/SelectionDialog;", "Landroid/app/Dialog;", "Landroid/view/View$OnClickListener;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "dialogMainBinding", "Lcom/example/kotlincrud/databinding/ViewSelectionBinding;", "mainViewModel", "Lcom/example/kotlincrud/viewmodel/MainViewModel;", "onClick", "", "view", "Landroid/view/View;", "app_debug"})
public final class SelectionDialog extends android.app.Dialog implements android.view.View.OnClickListener {
    private com.example.kotlincrud.databinding.ViewSelectionBinding dialogMainBinding;
    private com.example.kotlincrud.viewmodel.MainViewModel mainViewModel;
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public SelectionDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
}