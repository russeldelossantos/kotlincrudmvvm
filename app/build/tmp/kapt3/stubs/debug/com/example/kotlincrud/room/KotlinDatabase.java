package com.example.kotlincrud.room;

import java.lang.System;

@androidx.room.Database(version = 2, entities = {com.example.kotlincrud.model.Admin.class, com.example.kotlincrud.model.Customer.class, com.example.kotlincrud.model.Employee.class}, exportSchema = false)
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\bH&\u00a8\u0006\n"}, d2 = {"Lcom/example/kotlincrud/room/KotlinDatabase;", "Landroidx/room/RoomDatabase;", "()V", "getAdminDao", "Lcom/example/kotlincrud/dao/AdminDao;", "getCustomerDao", "Lcom/example/kotlincrud/dao/CustomerDao;", "getEmployeeDao", "Lcom/example/kotlincrud/dao/EmployeeDao;", "Companion", "app_debug"})
public abstract class KotlinDatabase extends androidx.room.RoomDatabase {
    private static volatile com.example.kotlincrud.room.KotlinDatabase INSTANCE;
    public static final com.example.kotlincrud.room.KotlinDatabase.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.kotlincrud.dao.AdminDao getAdminDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.kotlincrud.dao.EmployeeDao getEmployeeDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.kotlincrud.dao.CustomerDao getCustomerDao();
    
    public KotlinDatabase() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/example/kotlincrud/room/KotlinDatabase$Companion;", "", "()V", "INSTANCE", "Lcom/example/kotlincrud/room/KotlinDatabase;", "getDatabase", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.kotlincrud.room.KotlinDatabase getDatabase(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}