package com.example.kotlincrud.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J+\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u0002H\u0012H\u0002\u00a2\u0006\u0002\u0010\u0017J\u0012\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0\u0019J\u0012\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001d0\u001a0\u0019J\u0012\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001f0\u001a0\u0019J\u0010\u0010 \u001a\u00020!2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002J\u000e\u0010\"\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010#\u001a\u00020\u0011J!\u0010$\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u0002H\u0012\u00a2\u0006\u0002\u0010%R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\u0004R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/example/kotlincrud/viewmodel/MainViewModel;", "Landroidx/lifecycle/ViewModel;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "adminDialog", "Lcom/example/kotlincrud/dialogs/AdminDialog;", "getContext", "()Landroid/content/Context;", "setContext", "customerDialog", "Lcom/example/kotlincrud/dialogs/CustomerDialog;", "dao", "Lcom/example/kotlincrud/room/KotlinDatabase;", "employeeDialog", "Lcom/example/kotlincrud/dialogs/EmployeeDialog;", "dialogType", "", "T", "type", "", "key", "model", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V", "getAllAdmin", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/kotlincrud/model/Admin;", "getAllCustomer", "Lcom/example/kotlincrud/model/Customer;", "getAllEmployee", "Lcom/example/kotlincrud/model/Employee;", "isUpdate", "", "showSelectedDialog", "showSelectionDialog", "showUpdateDialog", "(Ljava/lang/String;Ljava/lang/Object;)V", "app_debug"})
public final class MainViewModel extends androidx.lifecycle.ViewModel {
    private final com.example.kotlincrud.room.KotlinDatabase dao = null;
    private com.example.kotlincrud.dialogs.AdminDialog adminDialog;
    private com.example.kotlincrud.dialogs.CustomerDialog customerDialog;
    private com.example.kotlincrud.dialogs.EmployeeDialog employeeDialog;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.kotlincrud.model.Admin>> getAllAdmin() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.kotlincrud.model.Customer>> getAllCustomer() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.kotlincrud.model.Employee>> getAllEmployee() {
        return null;
    }
    
    public final <T extends java.lang.Object>void showUpdateDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String type, T model) {
    }
    
    public final void showSelectionDialog() {
    }
    
    public final void showSelectedDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String type) {
    }
    
    private final <T extends java.lang.Object>void dialogType(java.lang.String type, java.lang.String key, T model) {
    }
    
    private final boolean isUpdate(java.lang.String key) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    public MainViewModel(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}