package com.example.kotlincrud.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u0006\u0010\u000e\u001a\u00020\u000bJ\u000e\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\u0004\u00a8\u0006\u0011"}, d2 = {"Lcom/example/kotlincrud/viewmodel/AdminDialogViewModel;", "Landroidx/lifecycle/ViewModel;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "adminRepository", "Lcom/example/kotlincrud/respository/AdminRepository;", "getContext", "()Landroid/content/Context;", "setContext", "addAdmin", "Lkotlinx/coroutines/Job;", "admin", "Lcom/example/kotlincrud/model/Admin;", "autoGenerateAdmin", "deleteAdmin", "updateAdmin", "app_debug"})
public final class AdminDialogViewModel extends androidx.lifecycle.ViewModel {
    private com.example.kotlincrud.respository.AdminRepository adminRepository;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job addAdmin(@org.jetbrains.annotations.NotNull()
    com.example.kotlincrud.model.Admin admin) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job updateAdmin(@org.jetbrains.annotations.NotNull()
    com.example.kotlincrud.model.Admin admin) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteAdmin(@org.jetbrains.annotations.NotNull()
    com.example.kotlincrud.model.Admin admin) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job autoGenerateAdmin() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    public AdminDialogViewModel(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}