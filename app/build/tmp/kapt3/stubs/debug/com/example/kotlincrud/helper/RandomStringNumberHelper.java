package com.example.kotlincrud.helper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/example/kotlincrud/helper/RandomStringNumberHelper;", "", "()V", "Companion", "app_debug"})
public final class RandomStringNumberHelper {
    public static final com.example.kotlincrud.helper.RandomStringNumberHelper.Companion Companion = null;
    
    public RandomStringNumberHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\b"}, d2 = {"Lcom/example/kotlincrud/helper/RandomStringNumberHelper$Companion;", "", "()V", "generateRandomCharacters", "", "lenght", "", "generateRandomNumbers", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String generateRandomCharacters(@androidx.annotation.IntRange()
        int lenght) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String generateRandomNumbers(@androidx.annotation.IntRange()
        int lenght) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}