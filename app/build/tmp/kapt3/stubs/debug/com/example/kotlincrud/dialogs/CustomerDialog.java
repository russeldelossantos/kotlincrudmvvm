package com.example.kotlincrud.dialogs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0006H\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u000e\u0010\u0017\u001a\u00020\u00142\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0018"}, d2 = {"Lcom/example/kotlincrud/dialogs/CustomerDialog;", "Landroid/app/Dialog;", "Landroid/view/View$OnClickListener;", "context", "Landroid/content/Context;", "type", "", "(Landroid/content/Context;Ljava/lang/String;)V", "customer", "Lcom/example/kotlincrud/model/Customer;", "customerDialogViewModel", "Lcom/example/kotlincrud/viewmodel/CustomerDialogViewModel;", "dialogMainBinding", "Lcom/example/kotlincrud/databinding/ViewCustomerDialogBinding;", "getType", "()Ljava/lang/String;", "fieldIsEmpty", "", "value", "onClick", "", "view", "Landroid/view/View;", "setTextField", "app_debug"})
public final class CustomerDialog extends android.app.Dialog implements android.view.View.OnClickListener {
    private com.example.kotlincrud.databinding.ViewCustomerDialogBinding dialogMainBinding;
    private com.example.kotlincrud.model.Customer customer;
    private com.example.kotlincrud.viewmodel.CustomerDialogViewModel customerDialogViewModel;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String type = null;
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void setTextField(@org.jetbrains.annotations.NotNull()
    com.example.kotlincrud.model.Customer customer) {
    }
    
    private final boolean fieldIsEmpty(java.lang.String value) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getType() {
        return null;
    }
    
    public CustomerDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String type) {
        super(null);
    }
}