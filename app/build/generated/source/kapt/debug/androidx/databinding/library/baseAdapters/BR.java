package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int adminDialogViewModel = 1;

  public static final int customerDialogViewModel = 2;

  public static final int employeeDialogViewModel = 3;

  public static final int selectionViewModel = 4;

  public static final int viewModel = 5;
}
