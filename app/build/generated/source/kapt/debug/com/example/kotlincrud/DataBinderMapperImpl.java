package com.example.kotlincrud;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.kotlincrud.databinding.ActivityMainBindingImpl;
import com.example.kotlincrud.databinding.ViewAdminDialogBindingImpl;
import com.example.kotlincrud.databinding.ViewCustomerDialogBindingImpl;
import com.example.kotlincrud.databinding.ViewEmployeeDialogBindingImpl;
import com.example.kotlincrud.databinding.ViewSelectionBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_VIEWADMINDIALOG = 2;

  private static final int LAYOUT_VIEWCUSTOMERDIALOG = 3;

  private static final int LAYOUT_VIEWEMPLOYEEDIALOG = 4;

  private static final int LAYOUT_VIEWSELECTION = 5;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(5);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.kotlincrud.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.kotlincrud.R.layout.view_admin_dialog, LAYOUT_VIEWADMINDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.kotlincrud.R.layout.view_customer_dialog, LAYOUT_VIEWCUSTOMERDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.kotlincrud.R.layout.view_employee_dialog, LAYOUT_VIEWEMPLOYEEDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.kotlincrud.R.layout.view_selection, LAYOUT_VIEWSELECTION);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_VIEWADMINDIALOG: {
          if ("layout/view_admin_dialog_0".equals(tag)) {
            return new ViewAdminDialogBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for view_admin_dialog is invalid. Received: " + tag);
        }
        case  LAYOUT_VIEWCUSTOMERDIALOG: {
          if ("layout/view_customer_dialog_0".equals(tag)) {
            return new ViewCustomerDialogBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for view_customer_dialog is invalid. Received: " + tag);
        }
        case  LAYOUT_VIEWEMPLOYEEDIALOG: {
          if ("layout/view_employee_dialog_0".equals(tag)) {
            return new ViewEmployeeDialogBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for view_employee_dialog is invalid. Received: " + tag);
        }
        case  LAYOUT_VIEWSELECTION: {
          if ("layout/view_selection_0".equals(tag)) {
            return new ViewSelectionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for view_selection is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(6);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "adminDialogViewModel");
      sKeys.put(2, "customerDialogViewModel");
      sKeys.put(3, "employeeDialogViewModel");
      sKeys.put(4, "selectionViewModel");
      sKeys.put(5, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(5);

    static {
      sKeys.put("layout/activity_main_0", com.example.kotlincrud.R.layout.activity_main);
      sKeys.put("layout/view_admin_dialog_0", com.example.kotlincrud.R.layout.view_admin_dialog);
      sKeys.put("layout/view_customer_dialog_0", com.example.kotlincrud.R.layout.view_customer_dialog);
      sKeys.put("layout/view_employee_dialog_0", com.example.kotlincrud.R.layout.view_employee_dialog);
      sKeys.put("layout/view_selection_0", com.example.kotlincrud.R.layout.view_selection);
    }
  }
}
